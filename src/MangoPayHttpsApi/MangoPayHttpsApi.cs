﻿using MangoPay.SDK;
using MangoPay.SDK.Core;
using MangoPay.SDK.Entities;
using MangoPay.SDK.Entities.GET;
using MangoPay.SDK.Entities.POST;
using MangoPay.SDK.Entities.PUT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangoPayHttps.Api
{
    public static class MangoPayHttpsApi
    {
        public static UserNaturalDTO CreateNaturalUser(UserNaturalPostDTO user, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            return api.Users.Create(user);
        }
        public static UserLegalDTO CreateLegalUser(UserLegalPostDTO user, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            return api.Users.Create(user);
        }
        public static WalletDTO CreateWallet(WalletPostDTO wallet, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            return api.Wallets.Create(wallet);
        }
        public static PayInCardWebDTO CreatePayIn(PayInCardWebPostDTO payin, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            return api.PayIns.CreateCardWeb(payin);
        }
        public static TransferDTO CreateTransfer(TransferPostDTO transfer, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            return api.Transfers.Create(transfer);
        }
        public static List<UserDTO> GetAllUsersDto(MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            var list = api.Users.GetAll(new Pagination() { Page = 1, ItemsPerPage = 100});

            if (list != null)
            {
                return list.ToList();
            }

            return null;
        }
        public static List<WalletDTO> GetAllUserWalletsDto(string userId, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);

            UserDTO usr = api.Users.Get(userId);

            if (usr != null)
            {
                var list = api.Users.GetWallets(userId, new Pagination());

                if (list != null)
                {
                    return list.ToList();
                }
            }

            return null;
        }
        public static UserLegalDTO GetLegalUserDto(string userId, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);
            return api.Users.GetLegal(userId);
        }
        public static void CreateKYCDocuments(List<KycDocumentTransferObject> documents, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);

            KycDocumentPutDTO updateDoc = new KycDocumentPutDTO()
            {
                Status = MangoPay.SDK.Core.Enumerations.KycStatus.VALIDATION_ASKED,
            };

            var groupedItems = from x in documents
                               group x by x.DocumentType into g
                               select new { DocumentType = g.Key, UserId = g.First().UserId, Items = g.ToList() };

            foreach (var groupedItem in groupedItems)
            {
                KycDocumentDTO retValue = api.Users.CreateKycDocument(groupedItem.UserId, groupedItem.DocumentType);
                
                foreach (var item in groupedItem.Items)
                {
                    api.Users.CreateKycPage(item.UserId, retValue.Id, item.BinaryData);
                }

                api.Users.UpdateKycDocument(groupedItem.UserId, updateDoc, retValue.Id);
            }
        }
        public static List<KycDocumentDTO> GetUserDocumentsDto(string userId, MangoPaySettings settings)
        {
            MangoPayApi api = new MangoPayApi();
            SetApiConfigSettings(settings, api);

            return api.Users.GetKycDocuments(userId, new Pagination() { ItemsPerPage = 100, Page = 1},
                new FilterKycDocuments()).ToList();
        }
        public static void SetApiConfigSettings(MangoPaySettings settings, MangoPayApi api)
        {
            api.Config = new Configuration()
            {
                ApiVersion = settings.ApiVersion,
                BaseUrl = settings.BaseUrl,
                ClientId = settings.ClientId,
                ClientPassword = settings.ClientPassword,
                Timeout = int.Parse(settings.Timeout),
            };
        }
    }
}
