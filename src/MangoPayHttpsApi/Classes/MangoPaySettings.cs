﻿
namespace MangoPayHttps.Api
{
    public class MangoPaySettings
    {
        public string ApiVersion { get; set; }
        public string BaseUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientPassword { get; set; }
        public string Timeout { get; set; }
    }
}
