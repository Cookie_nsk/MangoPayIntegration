﻿
using MangoPay.SDK.Core.Enumerations;

namespace MangoPayHttps.Api
{
    public class KycDocumentTransferObject
    {
        public string UserId { get; set; }
        public byte[] BinaryData { get; set; }
        public KycDocumentType DocumentType { get; set; }
    }
}
