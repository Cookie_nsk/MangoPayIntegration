﻿/*------validation and handling controls------*/
function validateFormData(sender, excludeDataCtrls) {
    var ctrls = $('#' + sender).find('[data-isRequired="true"]');
    var errors = 0;
    ctrls.each(function () {
        var type = this.getAttribute('data-ctrlType');
        var isAddr = this.getAttribute('data-isAddressCtrl');

        if (excludeDataCtrls && isAddr)
            return;

        if (type == 'textbox') {
            if (this.value.length == 0 || this.value.length > 255) {
                addClassValid(this, false);
                errors++;
            }
            else {
                addClassValid(this, true);
            }
        }
        if (type == 'datetime') {
            if (!validateDate($(this).val(), this)) {
                errors++;
            }
            else {
                addClassValid(this, true, true);
            }
        }
        if (type == 'dropdown') {
            if ($(this).val() == "-1") {
                addClassValid(this, false);
                errors++;
            }
            else {
                addClassValid(this, true);
            }
        }
        if (type == 'email') {
            if (!validateEmail($(this).val())) {
                addClassValid(this, false);
                errors++;
            }
            else {
                addClassValid(this, true);
            }
        }

    })

    return errors == 0;
}
function validateAddress(ctrlId) {
    var errors = 0;
    var arr = [];

    arr.push($("#" + ctrlId).find("#tbxLine1"));
    arr.push($("#" + ctrlId).find("#tbxCity"));
    arr.push($("#" + ctrlId).find("#tbxPostalCode"));
    arr.push($("#" + ctrlId).find("#ddlCountryAddr"));

    arr.forEach(function (item, i, arr) {
        var val = $(item).val();

        if (val.length == 0 || val == "-1") {
            addClassValid(item, false);
            errors++;
        }
        else {
            addClassValid(item, true);
        }
    });

    return errors == 0;
}
function clearValidationColors(sender) {
    var ctrls = $('#' + sender).find('[data-isRequired="true"]');

    ctrls.each(function () {
        var type = this.getAttribute('data-ctrlType');
        var elem = $(this);

        elem.removeClass("is-valid");
        elem.removeClass("is-invalid");

        if (type == 'datetime') {
            elem.parent().parent().parent().find(".invalid-feedback").hide();
        }
        else {
            elem.parent().parent().find(".invalid-feedback").hide();
        }
    })
}
function validateEmail(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}
function validateDate(value, sender) {
    
    var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
    var defaultErrorMessage = "This is not a valid date of birth";

    if (!dateRegex.test(value)) {
        showDateValidationError(sender, defaultErrorMessage);
        return false;
    }

    var currentDate = new Date(); 
    var minDate = new Date(1900, 00, 01);
    var overEighteen = new Date(currentDate.getFullYear() - 18, currentDate.getMonth(), currentDate.getDate());

    var date = value.split("/");
    var day = parseInt(date[0], 10), month = parseInt(date[1], 10) - 1, year = parseInt(date[2], 10);
    var inputDate = new Date(year, month, day);

    if (inputDate.getTime() > overEighteen.getTime()) {
        showDateValidationError(sender, "Minimum age 18");
        return false;
    }
    if (inputDate.getTime() < minDate.getTime()) {
        showDateValidationError(sender, defaultErrorMessage);
        return false;
    }

    return true;
}
function showDateValidationError(sender, message) {
    var elem = $(sender);
    elem.parent().parent().find(".invalid-feedback").html(message);
    elem.parent().parent().find(".invalid-feedback").show();
    elem.addClass("is-invalid");
    elem.removeClass("is-valid");
}
function addClassValid(sender, isValid, isDate) {
    var elem = $(sender);
    if (isValid) {
        elem.addClass("is-valid");
        elem.removeClass("is-invalid");

        if (isDate) {
            elem.parent().parent().parent().find(".invalid-feedback").hide();
            return;
        }
        elem.parent().parent().find(".invalid-feedback").hide();
    }
    else {
        elem.addClass("is-invalid");
        elem.removeClass("is-valid");

        if (isDate) {
            elem.parent().parent().parent().find(".invalid-feedback").show();
            return;
        }
        elem.parent().parent().find(".invalid-feedback").show();
    }
}
function toggleUserTypeControls(id1, id2, sender) {
    if (sender.checked) {
        $("#" + id1).show();
        $("#" + id2).hide();
    }
}
function validateDropDown(id) {
    if ($(id).val() == "-1") {
        addClassValid($(id), false);
        return false;
    }
    else {
        addClassValid($(id), true);
    }
    return true;
}
function validateFormDataWithUserTypeSelection(divId) {
    var ctrls = $('#' + divId).find('[data-isRequired="true"]');
    var errors = 0;
    ctrls.each(function () {
        var type = this.getAttribute('data-ctrlType');

        if (type == 'textbox') {
            if (this.value.length == 0 || this.value.length > 255) {
                addClassValid(this, false);
                errors++;
            }
            else {
                addClassValid(this, true);
            }
        }
        if (type == 'dropdown') {
            if ($(this).val() == "-1") {
                addClassValid(this, false);
                errors++;
            }
            else {
                addClassValid(this, true);
            }
        }
    })

    if ($("#cbxNatUser").prop("checked")) {
        if (!validateDropDown("#ddlNatUsrs")) {
            errors++;
        }
    }
    else if ($("#cbxLegalUsr").prop("checked")) {
        if (!validateDropDown("#ddlLegalUsrs")) {
            errors++;
        }
    }

    return errors == 0;
}
function toggleBtnText(sender) {
    if (sender.innerHTML.trim() == "Expand") {
        sender.innerHTML = "Collapse";
        return;
    }
    sender.innerHTML =  "Expand";
}
function validateIsDecimal(sender) {
    var inputVal = $(sender).val();
    var preDecimal = new RegExp(/^[-+]?[0-9]+\.+$/i);
    if (inputVal.length > 0 && !preDecimal.test(inputVal)) {

        var digit = new RegExp(/^[-+]?[0-9]+$/i);
        var decimal = new RegExp(/^[-+]?[0-9]+\.[0-9]+$/i);

        if (digit.test(inputVal) || decimal.test(inputVal)) {
            return true;
        }

        $(sender).val('');
    }
}
function validateIsPercentage(sender) {
    var inputVal = $(sender).val();
    if (inputVal.length > 0 ) {
        var digit = new RegExp(/^[-+]?[0-9]+$/i);
        if (digit.test(inputVal) && parseInt(inputVal) <= 100) {
            return true;
        }
        $(sender).val('');
    }
}
function validateIsNumber(sender) {
    var inputVal = $(sender).val();
    var digit = new RegExp(/^[-+]?[0-9]+$/i);
    return digit.test(inputVal);
}
function loadWalletsData(sender, url, tbxWalletId) {
    if ($(sender).val() != "-1") {

        $.ajax({
            url: url,
            type: "POST",
            data: { userId: $(sender).val() },
            beforeSend: setTextboxText(tbxWalletId, "Loading wallets data..."),
            success: function (data) {
                if (data.Result == "Ok") {
                    $("#" + tbxWalletId).val(data.WalletId);
                }
            }
        });
    }
    else {
        $("#" + tbxWalletId).val("Select user");
    }
}
function setTextboxText(tbxWalletId, tbxVal) {
    $("#" + tbxWalletId).val(tbxVal);
}
function updatePayingState() {
    $.ajax({
        url: "/Api/GetPayInState",
        type: "POST",
        success: function (data) {
            if (data.Result != undefined) {
                if (data.StopTimer != undefined) {
                    showAlert(data.Description, true);
                    clearInterval(document.PayinTimerlId);
                }
            }
        }
    });
    
}
function perfomApiRequest(postUrl, data) {
    $.ajax({
        url: postUrl,
        type: "POST",
        data: { jsonData: data },
        beforeSend: showAlert("Processing data, please wait", true),
        success: function (data) {
            if (data.Result == "Ok") {
                showAlert(data.Description, true);
                if (data.PayInUrl != undefined) {
                    window.open(data.PayInUrl, "_blank");
                    document.PayinTimerlId = setInterval(updatePayingState, 1000);
                }
            }
            else {
                showAlert(data.Description, false);
            }
        }
    });
}
function validateFileExtenstion(fileName) {
    var pattern = new RegExp(/^\.|\.jpg$|\.JPG|.png$|.pdf|.PDF|.jpeg|.JPEG|.bmp|.BMP/i);
    return pattern.test(fileName);
}