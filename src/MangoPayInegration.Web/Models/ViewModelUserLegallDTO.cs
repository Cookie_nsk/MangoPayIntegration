﻿using MangoPay.SDK.Core.Enumerations;
using MangoPay.SDK.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MangoPayInegration
{
    public class ViewModelUserLegallDTO
    {
        public CountryIso LegalRepresentativeCountryOfResidence { get; set; }
        public CountryIso LegalRepresentativeNationality { get; set; }
        public DateTime LegalRepresentativeBirthday { get; set; }
        public string LegalRepresentativeEmail { get; set; }
        public Address LegalRepresentativeAddress { get; set; }
        public string LegalRepresentativeLastName { get; set; }
        public string LegalRepresentativeFirstName { get; set; }
        public Address HeadquartersAddress { get; set; }
        public LegalPersonType LegalPersonType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CompanyNumber { get; set; }
        public string ShareholderDeclaration { get; set; }
        public string Tags { get; set; }

    }
}