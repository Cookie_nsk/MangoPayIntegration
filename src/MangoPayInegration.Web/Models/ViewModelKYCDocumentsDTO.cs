﻿using MangoPay.SDK.Core.Enumerations;
using MangoPay.SDK.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MangoPayInegration
{
    public class ViewModelKYCDocumentsDTO
    {
        public string UserId { get; set; }
        public PersonType UserType { get; set; }
        public string LegalPersonType { get; set; }
    }
}