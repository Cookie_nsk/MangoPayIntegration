﻿using MangoPay.SDK.Core.Enumerations;
using MangoPay.SDK.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MangoPayInegration
{
    public class ViewModelPayinDTO
    {
        public string AuthorId { get; set; }
        public string CreditedWalletId { get; set; }
        public double Amount { get; set; }
        public int Fees { get; set; }
        public CurrencyIso Currency { get; set; }
        public CardType CardType { get; set; }

    }
}