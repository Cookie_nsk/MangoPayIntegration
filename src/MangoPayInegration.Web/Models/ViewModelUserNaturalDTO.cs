﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MangoPay;
using MangoPay.SDK;
using MangoPay.SDK.Entities.POST;
using MangoPay.SDK.Core.Enumerations;
using MangoPay.SDK.Entities;

namespace MangoPayInegration
{
    public class ViewModelUserNaturalDTO
    {//MangoPay.SDK.Entities.POST.UserNaturalPostDTO - class doesn't have parametrless constructor
     //cant serialize Json from view.

        public int Capacity { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Address Address { get; set; }
        public DateTime Birthday { get; set; }
        public CountryIso Nationality { get; set; }
        public CountryIso CountryOfResidence { get; set; }
        public string Occupation { get; set; }
        public int? IncomeRange { get; set; }
        public string Tags { get; set; }
    }
}