﻿using MangoPay.SDK.Core.Enumerations;
using MangoPay.SDK.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MangoPayInegration
{
    public class ViewModelWalletDTO
    {
        public string Owners { get; set; }
        public string Description { get; set; }
        public CurrencyIso Currency { get; set; }

    }
}