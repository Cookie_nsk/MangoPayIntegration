﻿using MangoPayHttps.Api;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web;

namespace MangoPayInegration
{
    public static class AppSettingsHelper
    {
        public static MangoPaySettings GetSettings()
        {
            return new MangoPaySettings()
            {
                ApiVersion = ConfigurationManager.AppSettings["ApiVersion"],
                BaseUrl = ConfigurationManager.AppSettings["BaseUrl"],
                ClientId = ConfigurationManager.AppSettings["ClientId"],
                ClientPassword = ConfigurationManager.AppSettings["ClientPassword"],
                Timeout = ConfigurationManager.AppSettings["Timeout"],
            };
        }
        public static List<string> GetSupportedCurrencies()
        {
            return ConfigurationManager.AppSettings["SupportedCurrencies"]
                .Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}