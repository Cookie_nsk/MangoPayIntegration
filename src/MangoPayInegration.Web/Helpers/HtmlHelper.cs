﻿using MangoPay.SDK.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace MangoPayInegration
{
    public static class HtmlHelpers
    {
        public static T GetObjectFromEncodedJson<T>(string source, Type type) where T : class
        {
            return JsonConvert.DeserializeObject<T>(source, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
        }

        public static string ParseMPResponceError(ResponseException error)
        {
            StringBuilder sb = new StringBuilder();

            if (error != null)
            {
                sb.AppendFormat("Error: {0}<br />", error.ResponseError.Message);

                if (error.ResponseError != null && error.ResponseError.errors != null && error.ResponseError.errors.Count > 0)
                {
                    sb.Append("Additional info:<br />");
                    foreach (var item in error.ResponseError.errors)
                    {
                        sb.AppendFormat("<li>{0}</li>", item.Value);
                    }
                }
            }
            return sb.ToString();
        }
    }
}