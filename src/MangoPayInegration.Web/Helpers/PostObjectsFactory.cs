﻿using MangoPay.SDK.Core.Enumerations;
using MangoPay.SDK.Entities;
using MangoPay.SDK.Entities.POST;
using MangoPayHttps.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace MangoPayInegration
{
    public static class PostObjectsFactory
    {
        public static UserNaturalPostDTO GetNaturalUserPostDTO(ViewModelUserNaturalDTO viewModel)
        {
            UserNaturalPostDTO dto = new UserNaturalPostDTO(
                viewModel.Email,
                viewModel.FirstName,
                viewModel.LastName,
                viewModel.Birthday,
                viewModel.Nationality,
                viewModel.CountryOfResidence)
            {
                Address = viewModel.Address,
                Occupation = viewModel.Occupation,
                Tag = viewModel.Tags
            };

            if (viewModel.Capacity != -1)
            {
                dto.Capacity = (CapacityType)Enum.Parse( typeof(CapacityType), viewModel.Capacity.ToString());
            }
            if (viewModel.IncomeRange == -1)
            {
                dto.IncomeRange = null;
            }
            else
            {
                dto.IncomeRange = viewModel.IncomeRange;
            }

            return dto;
        }
        public static UserLegalPostDTO GetLegalUserPostDTO(ViewModelUserLegallDTO viewModel)
        {
            UserLegalPostDTO dto = new UserLegalPostDTO(
                viewModel.Email, 
                viewModel.Name, 
                viewModel.LegalPersonType,
                viewModel.LegalRepresentativeFirstName, 
                viewModel.LegalRepresentativeLastName,
                viewModel.LegalRepresentativeBirthday, 
                viewModel.LegalRepresentativeNationality,
                viewModel.LegalRepresentativeCountryOfResidence)
            {
                HeadquartersAddress = viewModel.HeadquartersAddress,
                LegalRepresentativeEmail = viewModel.LegalRepresentativeEmail,
                LegalRepresentativeAddress = viewModel.LegalRepresentativeAddress,
            };

            return dto;
        }
        public static WalletPostDTO GetWalletPostDTO(ViewModelWalletDTO viewModel)
        {
            return new WalletPostDTO(
                new List<string>() { viewModel.Owners },
                viewModel.Description,
                viewModel.Currency);

        }
        public static PayInCardWebPostDTO GetPayInCardWebDTO(ViewModelPayinDTO viewModel)
        {
            Money fees = new Money() { Currency = viewModel.Currency };
            fees.Amount = (long)((viewModel.Amount * 100) * (((double)viewModel.Fees) / 100));

            return new PayInCardWebPostDTO(
                viewModel.AuthorId,
                new Money() { Amount = (long)(viewModel.Amount * 100), Currency = viewModel.Currency },
                fees,
                viewModel.CreditedWalletId,
                ConfigurationManager.AppSettings["PayinPostBackUrl"],
                CultureCode.EN,
                viewModel.CardType
            );
        }
        public static TransferPostDTO GetTransferDTO(ViewModelTransferDTO viewModel)
        {
            Money fees = new Money() { Currency = viewModel.Currency };
            fees.Amount = (long)((viewModel.Amount * 100) * (((double)viewModel.Fees) / 100));

            return new TransferPostDTO(
                viewModel.AuthorId,
                viewModel.CreditedUserId,
                new Money() { Amount = (long)(viewModel.Amount * 100), Currency = viewModel.Currency },
                fees,
                viewModel.DebitedWalletId,
                viewModel.CreditedWalletId
            );
        }
        public static List<KycDocumentTransferObject> GetDocumentsList(ViewModelKYCDocumentsDTO viewModel)
        {
            List<KycDocumentTransferObject> retList = new List<KycDocumentTransferObject>();

            foreach (FileItem item in FilesHelper.FileItems.Where(x=>x.UserId == viewModel.UserId))
            {
                if (item.Pages != null && item.Pages.Count > 0)
                {
                    foreach (FileItem page in item.Pages)
                    {
                        retList.Add(CreateKycTransferObject(page.UserId, page.Guid, page.DocumentType));
                    }
                }
                else
                {
                    retList.Add(CreateKycTransferObject(viewModel.UserId, item.Guid, item.DocumentType));
                }
            }

            return retList;
        }
        private static KycDocumentTransferObject CreateKycTransferObject(string userId, string guid, KycDocumentType type)
        {
            return new KycDocumentTransferObject()
            {
                UserId = userId,
                BinaryData = FilesHelper.GetFileBytesByGuid(guid),
                DocumentType = type,
            };
        }
    }
}