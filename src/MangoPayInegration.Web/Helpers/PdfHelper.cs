﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using MangoPay.SDK.Core;
using MangoPay.SDK.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;

namespace MangoPayInegration
{
    public static class PdfHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseFile"></param>
        /// <param name="outputPath"></param>
        /// <returns>Returns new list of created pages names.</returns>
        public static List<string> SplitPfdIntoPages(HttpPostedFileBase baseFile, string outputPath)
        {
            List<string> retList = new List<string>();

            using (PdfReader reader = new PdfReader(baseFile.InputStream))
            {
                for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
                {
                    string pageName = string.Format("{0}_{1}", Guid.NewGuid(), pageNumber);

                    Document pdfPage = new Document();
                    PdfCopy copy = new PdfCopy(pdfPage, new FileStream(Path.Combine(outputPath, pageName), FileMode.Create));

                    pdfPage.Open();
                    copy.AddPage(copy.GetImportedPage(reader, pageNumber));
                    pdfPage.Close();

                    retList.Add(pageName);
                }
            }
            return retList;
        }
    }
}