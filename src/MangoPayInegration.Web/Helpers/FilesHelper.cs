﻿using MangoPay.SDK.Core;
using MangoPay.SDK.Core.Enumerations;
using NlogLogger;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace MangoPayInegration
{
    public static class FilesHelper
    {
        private static string _sessionFileItemsKey = "_fileUploadData";
        private static string ServerFilesOutputDirectory
        {
            get { return HttpContext.Current.Server.MapPath("~/Content/Uploads"); }
        }
        public static List<FileItem> FileItems
        {
            get
            {
                if (HttpContext.Current.Session[_sessionFileItemsKey] == null)
                {
                    HttpContext.Current.Session[_sessionFileItemsKey] = new List<FileItem>();
                }

                return HttpContext.Current.Session[_sessionFileItemsKey] as List<FileItem>;
            }
        }
        public static string SaveFile(HttpPostedFileBase file, KycDocumentType docType, string userId)
        {
            CheckIfDirectoryExists();
            string guid = Guid.NewGuid().ToString();

            file.SaveAs(Path.Combine(ServerFilesOutputDirectory, guid));
            FileItems.Add(CreateFileItem(guid, file.FileName, docType, userId, file));

            return guid;
        }
        public static void DeleteFile(string guid)
        {
            File.Delete(Path.Combine(ServerFilesOutputDirectory, guid));
            DeleteFileItemFromSession(guid);
        }
        public static string GetFileNameByGuid(string guid)
        {
            FileItem item = FileItems.FirstOrDefault(x => x.Guid.Equals(guid));

            if (item != null)
            {
                return item.FileName;
            }

            return string.Empty;
        }
        public static void ClearFileUploadData()
        {
            FileItems.Clear();

            try
            {
                foreach (string fileName in Directory.GetFiles(ServerFilesOutputDirectory))
                {
                    File.Delete(fileName);
                }
            }
            catch (Exception exc)
            {
                NlogHelper.LogError(exc);
            }
        }
        public static string ValidateUploadedDocuments(ViewModelKYCDocumentsDTO model)
        {
            StringBuilder sb = new StringBuilder();

            List<FileItem> idntProof = FileItems
                .Where(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.IDENTITY_PROOF).ToList();

            //valid if 2-o separate items, or there are 2-o pages exists in one item
            Func<List<FileItem>, bool> checkIdentityProof = (list) =>
                (list.Count() >= 2) || ((list.Count() >= 1) && (list.Any(z => z.Pages != null) && list.Any(z => z.Pages.Count >= 2)));

            if (FileItems != null)
            {
                if (model.UserType == PersonType.NATURAL)
                {
                    if (!checkIdentityProof(idntProof))
                    {
                        sb.Append("IDENTITY_PROOF document must be on two sides</br>");
                    }
                    if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.ADDRESS_PROOF) < 1)
                    {
                        sb.Append("ADDRESS_PROOF document is reqired</br>");
                    }
                }

                if (model.UserType == PersonType.LEGAL)
                {
                    if ((LegalPersonType)Enum.Parse(typeof(LegalPersonType), model.LegalPersonType) == LegalPersonType.BUSINESS)
                    {
                        if (idntProof.Count < 2)
                        {
                            sb.Append("IDENTITY_PROOF document must be on two sides</br>");
                        }
                        if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.ARTICLES_OF_ASSOCIATION) < 1)
                        {
                            sb.Append("ARTICLES_OF_ASSOCIATION document is reqired");
                        }
                        if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.REGISTRATION_PROOF) < 1)
                        {
                            sb.Append("REGISTRATION_PROOF document is reqired");
                        }
                        if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.SHAREHOLDER_DECLARATION) < 1)
                        {
                            sb.Append("SHAREHOLDER_DECLARATION document is reqired");
                        }
                    }

                    if ((LegalPersonType)Enum.Parse(typeof(LegalPersonType), model.LegalPersonType) == LegalPersonType.ORGANIZATION)
                    {
                        if (!checkIdentityProof(idntProof))
                        {
                            sb.Append("IDENTITY_PROOF document must be on two sides</br>");
                        }
                        if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.ARTICLES_OF_ASSOCIATION) < 1)
                        {
                            sb.Append("ARTICLES_OF_ASSOCIATION document is reqired");
                        }
                        if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.REGISTRATION_PROOF) < 1)
                        {
                            sb.Append("REGISTRATION_PROOF document is reqired");
                        }
                    }

                    if ((LegalPersonType)Enum.Parse(typeof(LegalPersonType), model.LegalPersonType) == LegalPersonType.SOLETRADER)
                    {
                        if (!checkIdentityProof(idntProof))
                        {
                            sb.Append("IDENTITY_PROOF document must be on two sides</br>");
                        }
                        if (FileItems.Count(x => x.UserId == model.UserId && x.DocumentType == KycDocumentType.REGISTRATION_PROOF) < 1)
                        {
                            sb.Append("REGISTRATION_PROOF document is reqired");
                        }
                    }
                }

            }

            return sb.ToString();
        }
        public static byte[] GetFileBytesByGuid(string guid)
        {
            using (FileStream file = File.Open(string.Format("{0}/{1}", ServerFilesOutputDirectory, guid), FileMode.Open))
            {
                byte[] buffer = new byte[file.Length];
                file.Read(buffer, 0, (int)file.Length);

                return buffer;
            }
        }
        private static FileItem CreateFileItem(string guid, string fileName, KycDocumentType type, string userId, HttpPostedFileBase baseFile)
        {
            FileItem fileItem = new FileItem()
            {
                UserId = userId,
                FileName = fileName,
                Guid = guid,
                DocumentType = type,
            };

            if (baseFile.FileName.ToLower().Contains(".pdf"))
            {
                fileItem.Pages = new List<FileItem>();

                List<string> pdfPagesNames = PdfHelper.SplitPfdIntoPages(baseFile, ServerFilesOutputDirectory);
                int index = 1;
                foreach (string pageName in pdfPagesNames)
                {
                    string baseFileName = baseFile.FileName.ToLower();
                    string pageFileName = baseFileName.Substring(0, baseFileName.LastIndexOf("."));

                    FileItem page = new FileItem()
                    {
                        UserId = userId,
                        FileName = string.Format("{0}_{1}_page.pdf", pageFileName, index),
                        Guid = pageName,
                        DocumentType = type,
                    };

                    fileItem.Pages.Add(page);
                    index++;
                }
            }

            return fileItem;
        }
        private static void DeleteFileItemFromSession(string guid)
        {
            if (HttpContext.Current.Session[_sessionFileItemsKey] == null)
            {
                return;
            }

            FileItem item = FileItems.FirstOrDefault(x => x.Guid.Equals(guid));

            if (item != null)
            {
                FileItems.Remove(item);
            }
        }
        private static void CheckIfDirectoryExists()
        {
            if (!Directory.Exists(ServerFilesOutputDirectory))
            {
                Directory.CreateDirectory(ServerFilesOutputDirectory);
            }
        }
    }
}