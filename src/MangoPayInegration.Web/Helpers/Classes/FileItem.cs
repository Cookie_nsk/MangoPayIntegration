﻿using MangoPayHttps.Api;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web;
using MangoPay.SDK.Core.Enumerations;

namespace MangoPayInegration
{
    public class FileItem
    {
        public string UserId { get; set; }
        public string FileName { get; set; }
        public string Guid { get; set; }
        public KycDocumentType DocumentType { get; set; }
        public List<FileItem> Pages { get; set; }
    }
}