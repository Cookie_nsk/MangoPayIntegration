﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MangoPayHttps.Api;
using MangoPay;
using MangoPay.SDK;
using MangoPay.SDK.Entities.POST;
using MangoPay.SDK.Core.Enumerations;
using System.Net;
using MangoPay.SDK.Entities.GET;
using MangoPay.SDK.Core;
using MangoPay.SDK.Entities;
using System.Text;
using System.Globalization;
using MangoPay.Db;
using NlogLogger;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MangoPayInegration.Controllers
{
    public class ApiController : Controller
    {
        [HttpPost]
        public ActionResult CreateNaturalUser(string jsonData)
        {
            ViewModelUserNaturalDTO model =
                HtmlHelpers.GetObjectFromEncodedJson<ViewModelUserNaturalDTO>(jsonData, typeof(ViewModelUserNaturalDTO));

            UserNaturalPostDTO postUser = PostObjectsFactory.GetNaturalUserPostDTO(model);
            UserNaturalDTO createdUser = null;

            try
            {
                createdUser = MangoPayHttpsApi.CreateNaturalUser(postUser, AppSettingsHelper.GetSettings());
                LogDataController.SaveLogData(createdUser, RequestType.Request);
            }
            catch (ResponseException exc)
            {
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("User creation failed. <br />{0}", HtmlHelpers.ParseMPResponceError(exc)) });
            }

            return Json(new { Result = "Ok", Description = string.Format("User successfuly created. User ID: {0}", createdUser.Id) });
        }
        [HttpPost]
        public ActionResult CreateLegalUser(string jsonData)
        {
            ViewModelUserLegallDTO model =
                HtmlHelpers.GetObjectFromEncodedJson<ViewModelUserLegallDTO>(jsonData, typeof(ViewModelUserLegallDTO));
            UserLegalPostDTO postUser = PostObjectsFactory.GetLegalUserPostDTO(model);
            UserLegalDTO createdUser = null;

            try
            {
                createdUser = MangoPayHttpsApi.CreateLegalUser(postUser, AppSettingsHelper.GetSettings());
                LogDataController.SaveLogData(createdUser, RequestType.Request);
            }
            catch (ResponseException exc)
            {
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("User creation failed. <br />{0}", HtmlHelpers.ParseMPResponceError(exc)) });
            }

            return Json(new { Result = "Ok", Description = string.Format("User successfuly created. User ID: {0}", createdUser.Id) });
        }
        [HttpPost]
        public ActionResult CreateWallet(string jsonData)
        {
            ViewModelWalletDTO model =
                HtmlHelpers.GetObjectFromEncodedJson<ViewModelWalletDTO>(jsonData, typeof(ViewModelWalletDTO));
            WalletPostDTO postWallet = PostObjectsFactory.GetWalletPostDTO(model);

            WalletDTO createdWallet = null;
            try
            {
                createdWallet = MangoPayHttpsApi.CreateWallet(postWallet, AppSettingsHelper.GetSettings());
                LogDataController.SaveLogData(model, RequestType.Request);
            }
            catch (ResponseException exc)
            {
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("Waller creation failed. <br />{0}", HtmlHelpers.ParseMPResponceError(exc)) });
            }

            return Json(new { Result = "Ok", Description = string.Format("Wallet successfuly created. Wallet ID: {0}", createdWallet.Id) });
        }
        [HttpPost]
        public ActionResult CreatePayIn(string jsonData)
        {
            Session["_payInState"] = null;
            PayInCardWebDTO createdPayin = null;
            try
            {
                ViewModelPayinDTO model = HtmlHelpers.GetObjectFromEncodedJson<ViewModelPayinDTO>(jsonData, typeof(ViewModelPayinDTO));
                PayInCardWebPostDTO postPayin = PostObjectsFactory.GetPayInCardWebDTO(model);

                LogDataController.SaveLogData(model, RequestType.Request);
                createdPayin = MangoPayHttpsApi.CreatePayIn(postPayin, AppSettingsHelper.GetSettings());
                LogDataController.SaveLogData(createdPayin, RequestType.Responce);
            }
            catch (ResponseException exc)
            {
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("Payin submition failed. <br />{0}", HtmlHelpers.ParseMPResponceError(exc)) });
            }
            catch (Exception exc)
            {
                NlogHelper.LogError(exc);
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("Payin submition failed. Error: {0}", exc.InnerException) });
            }

            if (createdPayin != null && createdPayin.Status == TransactionStatus.CREATED)
            {
                return Json(new { Result = "Ok", Description = "Payin in progress. Waiting for server to reply...", PayInUrl = createdPayin.RedirectURL });
            }
            else if (createdPayin != null && createdPayin.Status == TransactionStatus.FAILED)
            {
                return Json(new { Result = "Error", Description = createdPayin.ResultMessage });
            }

            return Json(new { Result = "Error", Description = "Internal server error. Error code: 500" });
        }
        public ActionResult PayInReturn(string transactionId)
        {
            Session["_payInState"] = true;
            return View("~/Views/Shared/Empty.cshtml");
        }
        [HttpPost]
        public ActionResult GetPayInState(string jsonData)
        {
            if (Session["_payInState"] != null && ((bool)Session["_payInState"]))
            {
                return Json(new { Result = "Succsess", Description = "Payin successfully created.", StopTimer = true });
            }

            return Json(new { InProgress = true });
        }
        public ActionResult CreateTransfer(string jsonData)
        {
            TransferDTO createdTransfer = null;
            try
            {
                ViewModelTransferDTO model = HtmlHelpers.GetObjectFromEncodedJson<ViewModelTransferDTO>(jsonData, typeof(ViewModelTransferDTO));
                TransferPostDTO postTransfer = PostObjectsFactory.GetTransferDTO(model);

                createdTransfer = MangoPayHttpsApi.CreateTransfer(postTransfer, AppSettingsHelper.GetSettings());
                LogDataController.SaveLogData(model, RequestType.Request);
            }
            catch (ResponseException exc)
            {
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("Transfer creation failed. <br />{0}", HtmlHelpers.ParseMPResponceError(exc)) });
            }
            catch (Exception exc)
            {
                NlogHelper.LogError(exc);
                LogDataController.SaveLogData(exc, RequestType.Responce);
                return Json(new { Result = "Error", Description = string.Format("Transfer creation failed. Error: {0}", exc.InnerException) });
            }

            if (createdTransfer != null)
            {
                if (createdTransfer.Status == TransactionStatus.FAILED)
                {
                    return Json(new { Result = "Error", Description = createdTransfer.ResultMessage });
                }

                return Json(new { Result = "Ok", Description = "Transfer successfuly created." });
            }

            return Json(new { Response = "Error", Description = "Internal server error. Error code: 500" });
        }
        [HttpGet]
        public static List<UserDTO> GetAllMangoPayUsers()
        {
            return MangoPayHttpsApi.GetAllUsersDto(AppSettingsHelper.GetSettings());
        }
        [HttpPost]
        public ActionResult GetUserWallet(string userId)
        {
            List<WalletDTO> wallets = MangoPayHttpsApi.GetAllUserWalletsDto(userId, AppSettingsHelper.GetSettings());

            if (wallets != null && wallets.Count > 0)
            {
                return Json(new { Result = "Ok", WalletId = wallets.First().Id });
            }
            return Json(new { Result = "Ok", WalletId = "User does not have any wallets" });
        }
        [HttpPost]
        public ActionResult GetUserLegalPersonType(string userId)
        {
            if (!userId.Equals("-1"))
            {
                UserLegalDTO userLegal = MangoPayHttpsApi.GetLegalUserDto(userId, AppSettingsHelper.GetSettings());

                if (userLegal != null)
                {
                    return Json(new { Result = "Ok", PersonType = userLegal.LegalPersonType.ToString() });
                }
                return Json(new { Result = "Error", PersonType = LegalPersonType.NotSpecified.ToString() });
            }

            return Json(new { Result = "Ok" });
        }
        [HttpPost]
        public ActionResult SubmitDocuments(string jsonData)
        {
            try
            {
                ViewModelKYCDocumentsDTO model =
                HtmlHelpers.GetObjectFromEncodedJson<ViewModelKYCDocumentsDTO>(jsonData, typeof(ViewModelKYCDocumentsDTO));

                string errors = FilesHelper.ValidateUploadedDocuments(model);

                if (errors.Length == 0)
                {
                    List<KycDocumentTransferObject> post = PostObjectsFactory.GetDocumentsList(model);

                    MangoPayHttpsApi.CreateKYCDocuments(post, AppSettingsHelper.GetSettings());
                    LogDataController.SaveLogData(model, RequestType.Request);
                    return Json(new { Result = "Ok", Description = "Documents uploaded to MangoPay" });
                }
                else
                {
                    return Json(new { Result = "Error", Description = errors });
                }

            }
            catch (Exception ex)
            {
                LogDataController.SaveLogData(ex, RequestType.Responce);
                return Json(new { Result = "Error", Description = ex.Message });
            }
        }
        [HttpPost]
        public ActionResult UploadFile(KycDocumentType docType, string userId)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string fileName = System.IO.Path.GetFileName(file.FileName);

                    return Json(new { Result = "Ok", FileName = fileName, Guid = FilesHelper.SaveFile(file, docType, userId) });
                }
                return Json(new { Result = "Ok", Description = "Please select a file." });
            }
            catch (Exception exc)
            {
                NlogHelper.LogError(exc);
                return Json(new { Result = "Ok", Description = exc.Message });
            }
        }
        [HttpPost]
        public ActionResult DeleteFile(string jsonData)
        {
            try
            {
                string fileName = FilesHelper.GetFileNameByGuid(jsonData);
                FilesHelper.DeleteFile(jsonData);
                return Json(new { Result = "Ok", Description = string.Format("File \"{0}\" deleted.", fileName) });
            }
            catch (Exception exc)
            {
                NlogHelper.LogError(exc);
                return Json(new { Result = "Ok", Description = exc.Message });
            }
        }
        [HttpPost]
        public ActionResult ClearFilesUploadData(string jsonData)
        {
            FilesHelper.ClearFileUploadData();
            return Json(new { Result = "Ok", Description = "Files upload data cleared" });
        }
        [HttpPost]
        public ActionResult GetUserDocuments(string userId)
        {
            List<KycDocumentDTO> docs = null;
            try
            {
                docs = MangoPayHttpsApi.GetUserDocumentsDto(userId, AppSettingsHelper.GetSettings());

                if (docs != null && docs.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (KycDocumentDTO doc in docs)
                    {
                        sb.AppendFormat(@"
                        <tr>
                            <td>{0}</td>
                            <td>{1}</td>
                            <td>{2}</td>
                        </tr>
                        ", doc.Type.ToString(),
                           doc.Status.ToString(),
                           doc.CreationDate.ToString(new CultureInfo("En-Gb")));
                    }

                    return Json(new { Result = "Ok", Items = sb.ToString() });
                }

                return Json(new { Result = "Ok", Items = "<tr><td colspan=\"3\">No results found.</td></tr>" });
            }
            catch (Exception exc)
            {
                return Json(new { Result = "Error", Description = string.Format("Load documents. Error: {0}", exc.InnerException) });
            }
        }
    }
}