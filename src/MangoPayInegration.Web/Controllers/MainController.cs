﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MangoPay;
using MangoPay.Db;
using MangoPay.SDK;

namespace MangoPayInegration.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Index()
        {
           return View();
        }
        public ActionResult Users()
        {
            return View();
        }
        public ActionResult Wallets()
        {
            return View();
        }
        public ActionResult KYCDocuments()
        {
            return View();
        }
        public ActionResult Transfers()
        {
            return View();
        }
        public ActionResult PayIns()
        {
            return View();
        }

    }
}