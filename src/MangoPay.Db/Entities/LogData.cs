using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace MangoPay.Db
{
    [Table("LogData")]
    public class LogData
    {
        [Key]
        public int Id { get; set; }
        public RequestType RequestType { get; set; }
        public string XmlData { get; set; }
        public string ObjectType { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}