using System;
using System.Data.Entity;
using System.Linq;

namespace MangoPay.Db
{
    public class MangoPayContext : DbContext
    {
        public MangoPayContext() : base("name=MangoPayContext")
        {
        }

        public virtual DbSet<LogData> LogData { get; set; }
    }

    
}