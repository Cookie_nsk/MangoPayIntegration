using NlogLogger;
using System;
using System.IO;
using System.Xml.Serialization;

namespace MangoPay.Db
{
    public static class LogDataController
    {
        public static void SaveLogData(object objToSave, RequestType requestType)
        {
            try
            {
                LogData data = new LogData()
                {
                    CreatedAt = DateTime.Now,
                    ObjectType = objToSave.GetType().ToString(),
                    RequestType = requestType,
                    XmlData = SerializeObject(objToSave)
                };

                using (MangoPayContext db = new MangoPayContext())
                {
                    db.LogData.Add(data);
                    db.SaveChanges();
                }
            }
            catch (Exception exp)
            {
                NlogHelper.LogError(exp);
            }
        }
        private static string SerializeObject(object rawObject)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(rawObject.GetType());

            using (StringWriter stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, rawObject);
                return stringWriter.ToString();
            }
        }
    }
}